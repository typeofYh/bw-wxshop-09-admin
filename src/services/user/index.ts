import { request } from '@umijs/max';

export const getNavData = () => request('/sys/menu/nav', {
  method: 'GET'
})