import { request } from '@umijs/max';

export const login = (data) => request('/login', {
  method: 'POST',
  data
})