import { PageContainer } from '@ant-design/pro-components';
import { Outlet, useRequest } from "umi"
import * as store from "store2"
import { getNavData }  from "@/services/user"
const HomeLayout = () => {
  const { data, loading } = useRequest(getNavData);
  if(loading) return <div>loading...</div>
  store.session('menulist', data);
  // 设置本地存储，存到session
  return (
    <PageContainer ghost>
      <Outlet />
    </PageContainer>
  )
}

export default HomeLayout;