import type { FC } from "react"
import { useRequest } from "@umijs/max"
import { useState, useMemo } from "react"
import { v4 } from 'uuid';
import { useNavigate } from 'umi';
import classNames from "classnames"
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { LoginForm, ProFormText } from "@ant-design/pro-components"
import * as store from "store2"
import { login } from "@/services/login"
import style from "./style.less"
const { BASE_URL } = process.env;

const Login:FC = () => {
  let navigate = useNavigate();
  const { run } = useRequest(login, {
    manual: true
  })
  const [sessionUUID, setSessionUUID] = useState(() => v4())
  const [imageCodeloading, setImageCodeloading] = useState(true);
  const imgUrl = useMemo(() => {
    return `${BASE_URL}/captcha.jpg?uuid=${sessionUUID}`
  }, [sessionUUID]);

  const handleChangeImageCode = () => { // 点击图片切换验证码
    setSessionUUID(v4());
    setImageCodeloading(true);
  }
  const handleShowImage = () => { // 图片加载成功隐藏loading
    setImageCodeloading(false);
  }
  const handleSubmit = async (val) => {
    const loginData = {...val, sessionUUID};
    try {
      const data = await run(loginData)
      // 把用户信息放入本地存储
      store.set('userInfo', {
        token: data.access_token,
        token_type: data.token_type
      })
      // 跳转页面
      navigate('/home', {replace: true})
    }catch(error) {
      handleChangeImageCode(); // 登录失败更新验证码
    }
  }
  return (
    <div>
      <LoginForm
        logo="https://www.bwie.net/style/images/logo.gif"
        title="八维电商后台管理系统"
        subTitle="管理商品信息、规格、用户等数据"
        onFinish={handleSubmit}
      >
        <ProFormText
          name="principal"
          fieldProps={{
            size: 'large',
            prefix: <UserOutlined className={'prefixIcon'} />,
          }}
          placeholder={'账号'}
          rules={[
            {
              required: true,
              message: '请输入账号！',
            },
          ]}
        />
        <ProFormText.Password
          name="credentials"
          fieldProps={{
            size: 'large',
            prefix: <LockOutlined className={'prefixIcon'} />,
          }}
          placeholder={'密码'}
          rules={[
            {
              required: true,
              message: '请输入密码！',
            },
          ]}
        />
        <div className={style.imageCodeBox}>
          <ProFormText
            name="imageCode"
            placeholder={'验证码'}
            fieldProps={{
              size: 'large'
            }}
            rules={[
              {
                required: true,
                message: '请输入验证码！',
              },
            ]}
          />
          <div className={classNames(style.imageCodePic, {
            [style.imageCodeLoading] : imageCodeloading
          })}>
            <img src={imgUrl} alt="验证码" onLoad={handleShowImage} onClick={handleChangeImageCode}/>
          </div>
        </div>
      </LoginForm>
    </div>
  )
}

export default Login;