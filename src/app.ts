// 运行时配置 代码运行时执行 在浏览器端执行
import { message } from 'antd';
import type { RequestConfig } from 'umi';
import * as store from "store2"
const whiteList = ['/login'];
// 全局初始化数据配置，用于 Layout 用户信息和权限初始化
// 更多信息见文档：https://next.umijs.org/docs/api/runtime-config#getinitialstate
export async function getInitialState(): Promise<{ name: string }> {
  return { name: '@umijs/max' };
}

export const layout = () => {
  return {
    logo: 'https://img.alicdn.com/tfs/TB1YHEpwUT1gK0jSZFhXXaAtVXa-28-27.svg',
    menu: {
      locale: false,
    },
  };
};
const addAuthorization = ({ headers, url }) => {
  const userInfo = store.get('userInfo');
  if(!whiteList.includes(url)){ // 要添加headers头
    return {
      ...headers,
      Authorization: userInfo && userInfo.token ? `${userInfo.token_type}${userInfo.token}` : ''
    }
  }
  return headers;
}
export const request: RequestConfig = {
  baseURL: process.env.BASE_URL,
  timeout: 10000,
  // other axios options you want
  errorConfig: {
    errorHandler(error){
      // 处理错误提示信息
      message.error(error.response.data);
    }
  },
  requestInterceptors: [
    (config) => {
      const headers = addAuthorization(config);
      return {
        ...config,
        headers
      }
    }
  ],
  responseInterceptors: []
};