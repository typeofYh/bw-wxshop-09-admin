# README

## 本地存储
只能通过浏览器窗口访问，服务器设置不了

window.localStorage： 本地永久存储，除非用户主动清楚
window.sessionStorage： 存储一次浏览器会话记录，窗口关闭浏览器自动释放内存
有封装的好的方法可以直接操作，window.localStorage.getItem(key)  window.localStorage.setItem(key,value)

在浏览器中通过document对象，在没有document对象的时候可以通过js对象模拟访问cookie
服务端可以操作cookie
document.cookie
cookie可以设置过期时间
在发送http请求的时候会自动在headers中携带cookie，所以cookie不能存储数据过多，否则会增加http传输大小