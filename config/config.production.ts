/*
 * @Author: Yh
 * @LastEditors: Yh
 * @info: 开发环境的环境变量
 */
export default {
  define: {
    "process.env": {
      BASE_URL: 'https://bjwz.bwie.com/mall4w',
      NODE_ENV: 'production',
      TIMEOUT: 1000
    },
  }
}