export default [
  {
    path: '/login',
    component: './Login',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
  },
  {
    path: '/',
    component: './layout',
    flatMenu: true,
    routes: [
      {
        name: '首页',
        path: 'home',
        component: './Home',
      }
    ]
  }
]