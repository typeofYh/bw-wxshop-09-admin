import { defineConfig } from '@umijs/max';
import routes from "./route"
// 构建时配置 webpack在编译时执行的 不是在浏览器端执行 node
export default defineConfig({ 
  antd: {},
  access: {},
  model: {},
  initialState: {},
  request: {
    dataField: '',
  },
  layout: {
    title: '八维电商小程序后台管理系统',
  },
  proxy:{
    '/api': {
      target: 'https://bjwz.bwie.com/mall4w',
      pathRewrite: {
        '/api': ''
      },
      changeOrigin: true,
    }
  },
  routes
});

