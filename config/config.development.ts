/*
 * @Author: Yh
 * @LastEditors: Yh
 * @info: 开发环境的环境变量
 */
export default {
  define: {
    "process.env": {
      BASE_URL: '/api',
      NODE_ENV: 'development',
      TIMEOUT: 1000
    },
  }
}